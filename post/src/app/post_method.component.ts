import { Component } from '@angular/core';
import { DataService } from './data.service';

@Component({
    selector:'post-method',
    templateUrl: "./post_method.component.html"
 
})

export class PostMethodComponent{
    comments:any[];
    comment = {
        name:'',
        email:'',
        body:''
    }

    constructor(
        public dataService:DataService
    )
    {
        this.dataService.getComment().subscribe(comments => {
            this.comments = comments;
        });
    }

    onSubmit(){
        this.dataService.addComment(this.comment).subscribe(comment => {
            this.comments.unshift(comment);
        });
    }
}
