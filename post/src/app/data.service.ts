
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService{
    

    constructor(public http:Http){

    }

    getComment(){
        return this.http.get('http://jsonplaceholder.typicode.com/comments')
            .map(res => res.json());
    }

    addComment(comment){
        return this.http.post('http://jsonplaceholder.typicode.com/comments', comment)
            .map(res => res.json());
    }

}